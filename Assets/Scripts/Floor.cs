﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
	public int CurrFloor = 1;
	public int DoorsPerFloor = 8;
	public GameObject DoorPrefab;
	public GameObject StairPrefab;
	public List<Door> Doors;

	private float _offset = 3.5f;
	private float _interval = 6f;
	private float _lastinterval = 8.5f;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < DoorsPerFloor; i++)
		{
			GameObject door = Instantiate(DoorPrefab);
			Door doorComp = door.GetComponent<Door>();
			door.transform.parent = gameObject.transform;
			door.transform.localPosition = new Vector3(_offset+_interval*i,1.17f,0);

			doorComp.Doornum = CurrFloor * 100 + i + 1;
			Doors.Add(doorComp);
		}

		GameObject stair = Instantiate(StairPrefab);
		Door stairComp = stair.GetComponent<Door>();
		stair.transform.parent = gameObject.transform;
		stair.transform.localPosition =
			new Vector3(_offset + _interval * (DoorsPerFloor - 1) + _lastinterval, 1.17f, 0);
		stairComp.IsStair = true;

		Doors.Add(stairComp);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
