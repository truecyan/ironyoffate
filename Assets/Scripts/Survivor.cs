﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Survivor : MonoBehaviour {

    // Use this for initialization
    public bool mc; // 시작 캐릭터만 true
    public string Name;
    public int bag_size;
    public int hunger; // 포만감
    public int thirst; // 갈증
    public int injured; // 0은 부상없음, 1은 작은 부상, 2는 큰 부상
    public bool sick; // True는 질병상태, false는 건강함
    public List<Items> bag;
    public int morale;
    public int fp_base; // 사기/부상 적용되지 않은 전투력
    public int fp; //전투력
    public Items equip;
	
	public Survivor()
    {
        mc = false;
        bag_size = 10;
        hunger = 100;
        thirst = 100;
        injured = 0; // 부상; 0은 부상없음, 1은 작은 부상, 2는 큰 부상, 3 이상이면 사망
        sick = false;
        bag = new List<Items>();
        morale = 50;
        fp_base = 10;
        equip = new Items();
        fp = fp_base;
    }

    public void reinitialize()
    {
        fp = ((fp_base + equip.power) * (50 + morale / 2) / 100)/(1+injured);
        if (sick)
        {
            fp = fp / 2;
        }
    }

    public void Use(Items item)
    {
        if (item.type == 0)
        {
            hunger = System.Math.Min(item.power+hunger,100);
        }
        else if (item.type == 1)
        {
            injured = System.Math.Max(0, injured -= item.power);
        }
        else if (item.type == 3)
        {
            if (equip.Name!="None")
            {
                bag.Add(equip);
                equip = item;
                reinitialize();
            }
            else
            {
                equip = item;
                reinitialize();
            }
        }
        if (item.type==0||item.type==1)
        {
            bag.Remove(item);
        }
    }
}
