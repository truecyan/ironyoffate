﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour {
    public string Name;
    public int type; // 식량: 0 치유: 1 도구: 2 무기: 3
    public int count; // 수량
    public int weight; // 무게
    public int power; // 식량일 경우 포만감, 치유일 경우 치유량
	// Use this for initialization
	public Items () {
        Name = "None";
        type = 3;
        count = 0;
        weight = 0;
        power = 0;
	}
	
    

}
