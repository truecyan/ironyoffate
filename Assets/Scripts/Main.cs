﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
	public static int PlayerFloor = 2;
	public static int MaxFloor = 10;
	public GameObject FloorPrefab;
	public Party party;
	public Text FloorText;


	// Use this for initialization
	void Start ()
	{
		GameObject currFloorObj = Instantiate(FloorPrefab);
		Floor currFloor = currFloorObj.GetComponent<Floor>();
		currFloor.CurrFloor = PlayerFloor;
		if (PlayerFloor > 1)
		{
			GameObject lowerFloorObj = Instantiate(FloorPrefab);
			Floor lowerFloor = lowerFloorObj.GetComponent<Floor>();
			lowerFloorObj.transform.Translate(0, -5.5f, 0);
			lowerFloor.CurrFloor = PlayerFloor-1;

		}
		if (PlayerFloor < MaxFloor)
		{
			GameObject upperFloorObj = Instantiate(FloorPrefab);
			Floor upperFloor = upperFloorObj.GetComponent<Floor>();
			upperFloorObj.transform.Translate(0, 5.5f, 0);
			upperFloor.CurrFloor = PlayerFloor+1;
		}

		FloorText.text = PlayerFloor + "F";
		//party = new Party();

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
