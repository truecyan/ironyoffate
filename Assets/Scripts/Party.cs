﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Party : MonoBehaviour {
    public List<Survivor> party;
    public Party()
    {
        party = new List<Survivor>();
        Survivor MC = new Survivor();
        MC.Name = "Bob";
        MC.mc = true;
        party.Add(MC);
    }
}
