﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door : MonoBehaviour
{
	public int Doornum = 0;
	public Text DoorText;

	public bool IsStair = false;
	// Use this for initialization
	void Start ()
	{
		if(!IsStair)
			DoorText.text = Doornum.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
