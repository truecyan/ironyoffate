﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Room : MonoBehaviour {

    public bool curtain;
    public bool dirty;
    public bool blackout;
    public bool water;
    public bool gore;
    public bool open;



	public Room()
    {
        curtain = false;
        dirty = false;
        blackout = false;
        water = false;
        gore = false;
        open = false;
        int init_case = Slot.spin();
        switch (init_case)
        {
            case 0:
                curtain = true;
                break;
            case 1:
                dirty = true;
                break;
            case 2:
                blackout = true;
                break;
            case 3:
                water = true;
                break;
            case 4:
                gore = true;
                break;
            case 5:
                open = true;
                break;
        }
    }
    public void search(Survivor survivor, List<Survivor> party)
    {
        int cur_case = Roulette.spin(0);
        switch(cur_case)
        {
            case 0: // 평화
                break;
            case 1: //생존자 조우
                break;
            case 2: //함정
                int luck = Random.Range(0, 100);
                if (luck<15)
                {
                    survivor.injured += 2;
                }
                else if (luck<35)
                {
                    survivor.injured += 1;
                }
                if (survivor.injured>2)
                {
                    party.Remove(survivor); //사망
                }
                break;
        }
    }
}
